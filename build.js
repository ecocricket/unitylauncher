var builder;
var Platform;
var child = require("child_process");
var promise = require("promise");


//child.exec("npm install electron-builder");
// Promise is returned
//console.log(Platform);

(function(){
  var ls = child.exec("npm install electron-builder");
    ls.stdout.pipe(process.stdout);
    ls.on('data', (data) => {
      console.log(data.toString("utf-8"))
    })
    ls.on('exit', () => {
      console.log("installed electron-builder");
      builder = require("electron-builder")
      Platform = builder.Platform;
      resolvePromises();
    })
})()

function winCustomInstall(){
  return new promise((resolve,reject)=>{
    builder.build({
      targets: Platform.WINDOWS.createTarget(),
      config: {
     
             "appId": "com.adomasgr.UnityLauncher",
             "productName": "UnityLauncher",
             "icon":"unity",
             "directories":{
                 "output":"dist/customInstaller"
             },
             "nsis":{
               "oneClick": false,
               "allowToChangeInstallationDirectory":true
             },
             "asar":true     
           }
      
     })
      .then(() => {
        console.log("winCustomBuildFinished");
        resolve(true);
      })
      .catch((error) => {
        reject(error);
      })
  })
}

function winOneClickInstall(){
  return new promise((resolve,reject)=>{
    builder.build({
      targets: Platform.WINDOWS.createTarget(),
      config: {
     
             "appId": "com.adomasgr.UnityLauncher",
             "productName": "UnityLauncher",
             "icon":"unity",
             "directories":{
                 "output":"dist/oneClickInstaller"
             },
             "nsis":{
               "oneClick": false,
               "allowToChangeInstallationDirectory":false
             },
             "asar":true     
           }
      
     })
      .then(() => {
        console.log("winOneClickBuildFinished")
        resolve(true);
      })
      .catch((error) => {
        reject(error);
      })
  })

}

function winOpenSourceInstall(){
  return new promise((resolve,reject)=>{
    builder.build({
      targets: Platform.WINDOWS.createTarget(),
      config: {
          "appId": "com.adomasgr.UnityLauncher",
          "productName": "UnityLauncher",
          "icon":"unity",
          "directories":{
              "output":"dist/openSource"
          },
          "win":{
            "target":"dir"
          },
          "asar":false
          }
     })
      .then(() => {
        console.log("winCustomBuildFinished");
        resolve(true);
      })
      .catch((error) => {
        reject(error);
      })
  })
}


function resolvePromises(){
  winCustomInstall().then(()=>{
    winOneClickInstall().then(()=>{
      winOpenSourceInstall().then(()=>{
        console.log("ALL BUILDS FINISHED SUCCESFULLY");
        //uninstall();
      }).catch((err)=>{console.log(err)})     
    }).catch((err)=>{console.log(err)})
  }).catch((err)=>{console.log(err)})
}

function uninstall(){
  var ls = child.exec("npm uninstall electron-builder");
  ls.stdout.pipe(process.stdout);
  ls.on('data', (data) => {
    console.log(data.toString("utf-8"))
  })
  ls.on("exit",() => {
    console.log("electron-builder uninstalled");
  })
}


// winCustomInstall().catch((err)=>{
//   console.log(err)
//   winOneClickInstall().catch((err)=>{
//     console.log(err);
//   })
// })




 //child.exec("npm uninstall electron-builder");