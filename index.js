const electron = require('electron')
const {app, BrowserWindow, Tray, Menu, autoUpdater} = electron;
const path = require('path')
const fs   = require('fs');
const url = require('url')
const log     = require("electron-log");

const windowStateKeeper = require('electron-window-state');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win

function createWindow () {
  // Create the browser window.
  
  let mainWindowState = windowStateKeeper({
    defaultWidth:1200,
    defaultHeight:800
  });

  win = new BrowserWindow({
    'x': mainWindowState.x,
    'y': mainWindowState.y,
    'width': mainWindowState.width,
    'height': mainWindowState.height
  })
  //limit to one instance

  var shouldQuit = app.makeSingleInstance(function(commandLine, workingDirectory) {
    // Someone tried to run a second instance, we should focus our window.
    if (win) {
      if (win.isMinimized()) win.restore();
      win.focus();
      win.show();
    }
  });
  
  if (shouldQuit) {
    app.quit();
    return;
  }

  //win.setSize(1200,800);

  mainWindowState.manage(win);

  // and load the index.html of the app.
  win.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }))

  // Open the DevTools.
  //win.webContents.openDevTools()  //<- DEV TOOLS

  var appIcon = new Tray(path.join(__dirname,'unity.ico'));
  var contextMenu = Menu.buildFromTemplate([
    {
      label:'Open',click:function(){
        win.show();
      }
    },
    {
      label:"Quit",click:function(){
        app.isQuiting = true;
        app.quit();
      }
    }
  ])
  // Emitted when the window is closed.
  appIcon.setContextMenu(contextMenu);

  appIcon.on("double-click",()=>{
    win.show();
  })

  win.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    
    win = null
  })
  win.on('close',(event)=>{
      if(!app.isQuiting){
        event.preventDefault()
        win.hide();
    }
  })

  win.on('minimize',(event)=>{
    // event.preventDefault();
    // win.hide();
    //
  })

  win.on('show',()=>{
    appIcon.setHighlightMode('always');
  })
  setupUpdater();
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

function setupUpdater(){
  
  return;

  var path = "C:\Users\EcoCricket\Desktop\dirs"
  log.info("starting")

  try{
    autoUpdater.setFeedURL(path);
    autoUpdater.checkForUpdates()

    autoUpdater.on("error",(e)=>{
      log.info("error");
      log.info(e);
    })
    autoUpdater.on("checking-for-update",(e)=>{
      log.info("checked for update");
      log.info(e);
    })
  }catch(e){
    log.info(e);
  }
  
}