var fs = require("fs");
var p  = require("path");
console.time("time");
basePaths = ["D:\\UnityVersions"];

found = [];
notFound = [];

basePaths.forEach(function(base){
    
    dirs = fs.readdirSync(base)

        dirs.forEach(function (element) {

            //for unity 5.0 and up
            var check1 = p.join(base,element,"Editor\\Unity.exe");
            //for unity 4.7 and lover
            var check2 = p.join(base,element,"Unity.exe");
            
            if (fs.existsSync(check1)) {
                found.push(p.join(base, element));
            } else if (fs.existsSync(check2)) {
                found.push(p.join(base, element));
            } else {
                notFound.push(p.join(base, element));
            }
            
        });

})

console.log(notFound);
console.log(found);
console.timeEnd("time")
