var fs = require("fs");
var _path = require("path");
const _dir = _path.dirname(_path.dirname(_path.dirname(__dirname)));

angular.module("App")

.service("Globals",function(){
    this.unity = [];
    this.searchPaths = [];
    this.projectPaths = [];
    // for creating unity with keys
    this.unityWithKeys = [];

    //save to config info about unitys
    this.save = function(){
        //setDataFromUnityWithKeys();
        var tmp = {
            unity:this.unity,
            searchPaths:this.searchPaths,
            projectPaths:this.projectPaths,
        };
        fs.writeFileSync(_dir+"\\config\\unityPaths.json",JSON.stringify(tmp));
    }
    //updating variables to current saved json
    this.update = function(){
       var tmp = fs.readFileSync(_dir+"\\config\\unityPaths.json","utf-8");
       tmp = JSON.parse(tmp);
       this.unity = tmp.unity;
       this.searchPaths = tmp.searchPaths;
       this.projectPaths = tmp.projectPaths;
       //createUnityWithKeys(this.unity);
    }
    //converting array type to object type
    function createUnityWithKeys(unitys){
        var res = {};
        
        unitys.forEach(function(unity) {
            tmp = unity.version.replace(/(Unity)|(\(32-bit\)|\(64-bit\)$)|\s/g,"");
            res[tmp] = unity;
        });

        this.unityWithKeys = res;
    }

    //converting object type to array type before saving to config/
    function setDataFromUnityWithKeys(){
        var modified = [];
        for(var key in this.unityWithKeys){
            value = this.unityWithKeys[key];
            delete value.$$hashKey;
            modified.push(value);
        }

        this.unity = modified;

    }
})