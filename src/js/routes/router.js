angular.module("App", ["ngRoute", "luegg.directives", "ngTable"])

    .config(function ($routeProvider) {
        $routeProvider
            .when("/", {
                //controller:"initCtrl"
                //templateUrl:"src/html/test.html"
            })
            .when("/main", {
                controller: "versionsCtrl",
                templateUrl: "src/html/main.html"

                //templateUrl:"src/html/test.html"
            })
            .when("/searchUnity", {
                templateUrl: "src/html/searchUnity.html"
            })
            .when("/searchProjects", {
                templateUrl: "src/html/searchProjects.html"
            })
            .otherwise({
                redirectTo: '/main'
            })
    })