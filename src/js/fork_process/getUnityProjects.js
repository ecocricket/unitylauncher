var fs = require("fs");
var pathutil = require("path");
const _dir = pathutil.dirname(pathutil.dirname(pathutil.dirname(__dirname)));

var unityJSON = fs.readFileSync(pathutil.join(_dir,"config/unityPaths.json"),"utf-8");
unityJSON = JSON.parse(unityJSON);

var paths = unityJSON.projectPaths;
var projects = []; 

paths.forEach((path)=>{
    
    var dirs = fs.readdirSync(path);
    dirs.forEach((proj)=>{
        //
        
        if(fs.existsSync(pathutil.join(path,proj,"Assets"))){

            var lastOpenedWith = getVersion(pathutil.join(path,proj));
            var lastModified   = getModifiedTime(pathutil.join(path,proj));

            projects.push({
                path:pathutil.join(path,proj),
                version:lastOpenedWith,
                lastModified:lastModified,
                title:proj
            })
        }

    });
});

    process.send(JSON.stringify(projects));    




//very experimental
function getVersion(path){
    if(fs.existsSync(pathutil.join(path,"ProjectSettings/ProjectVersion.txt"))){
        
        var str = fs.readFileSync(pathutil.join(path,"ProjectSettings/ProjectVersion.txt"),"utf-8");
        var indexes = [];
        indexes = str.split("m_");
        var version = [];
        
        indexes.forEach(function(el){

            if(el.match(/EditorVersion/gi)){
                version = el.match(/:(.*)/);
            }
            
        })
        return version[1] || "Something went wrong"
        
    }else{
        return "Version not found"
    }

    
}

function getModifiedTime(path){
    var stats = fs.statSync(path);
    var mtime = new Date(stats.mtime).getTime();
    //var mtime = new Date(util.inspect(stats.mtime));
    //console.log(mtime);
    return mtime;
}
