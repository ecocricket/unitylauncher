const fs = require("fs");
const pathutil = require("path");

//this is for UNITY modules, not node >:p
module.exports = unityModuleGetter = function(parentPath){
   
    var exist = fs.existsSync(pathutil.join(parentPath,"Data\\PlaybackEngines"));
    if(!exist) return;
    
    var dirs = fs.readdirSync(pathutil.join(parentPath,"Data\\PlaybackEngines"));

    let unityModules = [];
    dirs.forEach(unityModule=>{
        //windows standalone
        if(unityModule.match(/windowsstandalonesupport/i)){
            unityModules.push({
                name:"Windows",
                path:pathutil.join(parentPath,"Data\\PlaybackEngines",unityModule),
                color:"#247BA0",
                version:"N/A"
            })
        
        }
        //MAC 
        else if(unityModule.match(/macstandalonesupport/i)){
            unityModules.push({
                name:"Mac",
                path:pathutil.join(parentPath,"Data\\PlaybackEngines",unityModule),
                color:"#247BA0",
                version:"N/A"
            })
        }
        //LINUX
        else if(unityModule.match(/linuxstandalonesupport/i)){
            unityModules.push({
                name:"Linux",
                path:pathutil.join(parentPath,"Data\\PlaybackEngines",unityModule),
                color:"#247BA0",
                version:"N/A"
            })
        } 
        //ANDROID
        else if(unityModule.match(/AndroidPlayer/i)){
            unityModules.push({
                name:"Android",
                path:pathutil.join(parentPath,"Data\\PlaybackEngines",unityModule),
                color:"#FF4073",
                version:"N/A"
            })
        }
        //VUFORIA
        else if(unityModule.match(/vuforiasupport/i)){
            unityModules.push({
                name:"Vuforia",
                path:pathutil.join(parentPath,"Data\\PlaybackEngines",unityModule),
                color:"#FF4073",
                version:"N/A"
            })
        }
        //TIZEN
        else if(unityModule.match(/TizenPlayer/i)){
            unityModules.push({
                name:"Tizen",
                path:pathutil.join(parentPath,"Data\\PlaybackEngines",unityModule),
                color:"#FF4073",
                version:"N/A"
            })
        }
        //iOS
        else if(unityModule.match(/iossupport/i)){
            unityModules.push({
                name:"iOS",
                path:pathutil.join(parentPath,"Data\\PlaybackEngines",unityModule),
                color:"#FF4073",
                version:"N/A"
            })
        }
        //BLACKBERRY
        else if(unityModule.match(/blackberrysupport|blackberryplayer/i)){
            unityModules.push({
                name:"BlackBerry",
                path:pathutil.join(parentPath,"Data\\PlaybackEngines",unityModule),
                color:"#FF4073",
                version:"N/A"
            })
        } 
        //WindowsPhone
        else if(unityModule.match(/wp8support|wp81support|wp10support/i)){
            unityModules.push({
                name:"WindowsPhone",
                path:pathutil.join(parentPath,"Data\\PlaybackEngines",unityModule),
                color:"#FF4073",
                version:"N/A"
            })
        }
        //METRO & UWP 
        //TODO: (sita gal dar pakeist reiktu. klausk deniso del folderiu)
        else if(unityModule.match(/metrosupport/i)){
            unityModules.push({
                name:"Metro/UWP",
                path:pathutil.join(parentPath,"Data\\PlaybackEngines",unityModule),
                color:"#247BA0",
                version:"N/A"
            })
        }
        //WebPlayer
        else if(unityModule.match(/WebPlayer/i)){
            unityModules.push({
                name:"WebPlayer [deprecated]",
                path:pathutil.join(parentPath,"Data\\PlaybackEngines",unityModule),
                color:"#5D081F",
                version:"N/A"
            })
            
        }
        //WebGL
        else if(unityModule.match(/webglsupport/i)){
            unityModules.push({
                name:"WebGL",
                path:pathutil.join(parentPath,"Data\\PlaybackEngines",unityModule),
                color:"#50514F",
                version:"N/A"
            })
        }
        //SAMSUNG TV
        else if(unityModule.match(/stvplayer/i)){
            unityModules.push({
                name:"SamsungTV",
                path:pathutil.join(parentPath,"Data\\PlaybackEngines",unityModule),
                color:"#50514F",
                version:"N/A"
            })
        }
        //APPLE TV
        else if(unityModule.match(/AppleTVSupport/i)){
            unityModules.push({
                name:"AppleTV",
                path:pathutil.join(parentPath,"Data\\PlaybackEngines",unityModule),
                color:"#50514F",
                version:"N/A"
            })
        }
        //FACEBOOK
        else if(unityModule.match(/Facebook/i)){
            unityModules.push({
                name:"Facebook",
                path:pathutil.join(parentPath,"Data\\PlaybackEngines",unityModule),
                color:"#50514F",
                version:"N/A"
            })
        }
        //XBOX ONE 
        else if(unityModule.match(/XboxOnePlayer/i)){
            unityModules.push({
                name:"XBOX ONE",
                path:pathutil.join(parentPath,"Data\\PlaybackEngines",unityModule),
                color:"#107C0F",
                version:"N/A"
            })
        }
        else{
            unityModules.push({
                name:"un::" + unityModule,
                path:pathutil.join(parentPath,"Data\\PlaybackEngines",unityModule),
                color:"#247BA0",
                version:"N/A"
            })
        }

    })

    return unityModules;
}