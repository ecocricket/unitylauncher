var child = require("child_process");
var promise = require("promise");
var event  = require("events");
var pathutil = require("path")
var emitter = new event.EventEmitter();
const _dir = pathutil.dirname(pathutil.dirname(pathutil.dirname(__dirname)));
/**
 * start function creates new process instance
 * events can send info about its current status
 */
module.exports = self = {
    start:main,
    events:emitter  
}

function main(){
    var ls = child.fork(pathutil.join(_dir, "src/js/fork_process/getUnityProjects.js"));
    
    ls.on('message',(msg)=>{
        emitter.emit("data", msg);
    });
    ls.on('close',()=>{
        emitter.emit('close');
    })
    ls.on('error',(err)=>{
        emitter.emit('error', err);
    })

  
}
