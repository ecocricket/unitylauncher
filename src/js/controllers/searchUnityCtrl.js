var walk = require('walk'),
    fs = require('fs'),
    walker;
var promise = require("promise");
var pathutil = require("path");
var child = require("child_process");
var unityModuleGetter = require("../nodejs/unityModuleGetter");
const _dir = pathutil.dirname(pathutil.dirname(pathutil.dirname(__dirname)));

angular.module("App")
    /**
     * main controller for the search view
     */
    .controller("searchUnityCtrl", function ($scope, $window, $http, Globals) {
        $scope.options;
        $scope.paths;
        $scope.dataToShow = [];

        if (Globals.searchPaths) {
            $scope.dataToShow = Globals.searchPaths;
            $scope.$evalAsync();
        }

        (function () {

            $http({
                method: 'GET',
                url: 'setup.json'
            })
                .then(function (data) {
                    $scope.options = data.data;
                    if ($scope.options.debug) {
                        //debugger;
                        if ($scope.options.filename && (typeof $scope.options.skipDirectories == "object") && (typeof $scope.options.filename == "string")) {

                            console.log("setup.json is valid");
                        } else {
                            console.error("setup.json is not valid");
                        }
                    }
                    console.log($scope.options);
                })
                .catch(function (error) {
                    toastr.error("Could not get setup.json file!, some functions will not work!");
                    toastr.error(error);
                });

        })()

        /**
         * once paths have been added, and search button  is clicked, search will start
         */
        $scope.searchWithPaths = function () {
            //debugger;
            //$scope.all_output = "TEST";
            $scope.paths = $window.paths;
            var loopCount = $scope.paths.length;
            var walker;
            var res = {};
            var resPaths = [];
            var STOP = false; //also non working, need to implement it someday
            $scope.found_output = "";
            $scope.count1 = 0;
            var skipDirectories = $scope.options.skipDirectories; //does nothing as for now

            if($scope.paths.length<1){
                return toastr.error("please add paths where unity is installed");
            }
            //loop for the paths provided
            // setting timeout so UI could load in view without lag:)
            setTimeout(function(){
                return new promise(function (resolve, reject) {
                    var cnt  = 0;
                    var countDown = $scope.paths.length;

                    $scope.paths.forEach(function (base) {

                        var dirs = fs.readdirSync(base)
                        
                        dirs.forEach(function (element) {

                            //for unity 5.0 and up
                            var check1 = pathutil.join(base, element, "Editor\\Unity.exe");
                            //for unity 4.7 and lower
                            var check2 = pathutil.join(base, element, "Unity.exe");

                            if (fs.existsSync(check1)) {
                                resPaths.push(pathutil.join(base, element, "Editor"));
                                // sending to UI
                                $scope.found_output += pathutil.join(base, element, "Editor") + "\n";
                                $scope.count1++;
                                
                            } else if (fs.existsSync(check2)) {
                                resPaths.push(pathutil.join(base, element));
                                // sending to UI
                                $scope.found_output += pathutil.join(base, element) + "\n";
                                $scope.count1++;
                            }

                            $scope.$evalAsync();
                           

                        });
                        cnt++;
                        
                        toastr.success("search finnished part(" + (cnt) + "/" + $scope.paths.length + ")");
                        // if all paths have been iterated, resolve promise
                        countDown--;
                        if(countDown<=0){
                            $scope.found_output += "Search finnished\nGetting Unity properties...\n";
                            $scope.searchProgressBar={
                                'width':'60%'
                            };
                            $scope.$evalAsync();
                            resolve("Getting properties");
                        }
                        

                    })
                }).then(function (info) {

                    //resPaths = resPaths.concat(tmp);
                    console.log(resPaths);
                    res.unity = resPaths;
                    res.searchPaths = $scope.paths;

                    if(res.unity.length<1){
                        $scope.found_output += "ERROR: no unity found with provided paths";
                        $scope.stop_button = "Close";
                        $scope.stopBtnClass = "btn btn-success form-control search-btn"
                        setTimeout(()=>{
                            $scope.progressBarWrapper = {
                                "display":"none"
                            };
                            $scope.$evalAsync();
                        },1000); 
                        $scope.$evalAsync();
                        $window.stopScroll();
                        return toastr.error("Unity not found");

                     }
                    /**
                     * get versions is function that uses powershell to get all the needed iformation about the Uninstall.exe (unity)
                     */
                    /**
                     * @param resPaths = all unity paths
                     * @param searchPaths = paths that was used to search unity
                     */
                    
                    getVersions({unity:resPaths,searchPaths:$scope.paths})
                        .then((result) => putDataIntoJSON(res.searchPaths, result)).catch(logError);


                    toastr.success(info);
                    

                }).catch(logError);
            },1000);
            // setTimeout(function(){
            //     //console.log(search);
            //     STOP = true;

            // },5000);


        }
        /**
         * get versions is function that uses powershell to get all the needed iformation about the Uninstall.exe (unity)
         */
        function getVersions(res) { //powershell -command "& {&Get-ItemPropertyValue 'D:\unityVersions\unity1 - Copy (4)\Editor\Uninstall.exe' -name 'VersionInfo' | ConvertTo-Json}"
            return new promise(function (resolve, reject) {

                var asyncOperations = res.unity.length;
                var result = [];
                //progress bar variables
                var progressStartVal = 60;
                var progressVal = 40/asyncOperations;

                res.unity.forEach(function (element) {

                    if (fs.existsSync(pathutil.join(element, "Uninstall.exe"))) {

                        var command = 'powershell -command "& {&Get-ItemPropertyValue -Path \'' + element + "\\Uninstall.exe" + '\' -name \'VersionInfo\' | ConvertTo-Json}"';
                        //console.log(command);
                        child.exec(command, function (error, ls, stderr) {

                            if (error) {
                                console.error(error);
                                reject(error);
                            } //this is the error on our side
                            //this is the error that will be outputed if command is invalid or sm sht
                            if (stderr) {
                                console.error(stderr);
                                console.warn("Command that was executed when error happened:\n" + command);
                                reject(stderr);
                            }

                            ls = ls.toString();
                            ls = ls.replace(/\n|\r/g, "");
                            if (ls == "") {
                                toastr.warn("info about unity was returned empty (unity will not be visible as installed)\n " + element);
                                asyncOperations--;
                                if (asyncOperations <= 0) {

                                    resolve(result);
                                } else {
                                    return
                                }

                            };
                            ls = JSON.parse(ls);
                            /**
                             * check what modules are installed
                             */
                            let modules = unityModuleGetter(element);
                            

                            result.push({
                                parentPath: element,
                                executablePath: pathutil.join(element, "Unity.exe"),
                                path: ls.FileName,
                                rawVersion: ls.ProductName,
                                version:ls.ProductName.replace(/(Unity)|(\(32-bit\)|\(64-bit\)$)|\s/g,""),
                                modules: modules
                                    /*{
                                        name:"Android"
                                        path:"...Editor"
                                        version:"N/A" //maybe someday for pacman
                                    }*/
                                
                            });
                            $scope.found_output += "Scanned files for properties ("+((res.unity.length-asyncOperations)+1)+"/"+res.unity.length+")\n";
                            //progress bar
                            progressStartVal+=progressVal;
                            $scope.searchProgressBar={
                                'width':progressStartVal+'%'
                            }

                            

                            asyncOperations--;
                            if (asyncOperations <= 0) {
                                $scope.found_output += "FINISHED!";
                                $scope.$evalAsync();
                                resolve(result);
                                
                            }
                            $scope.$evalAsync();
                        })

                    } else {
                        toastr.error(element + "\\Uninstall.exe does not exists");
                        asyncOperations--;
                        if (asyncOperations <= 0) {
                            $scope.found_output += "FINISHED!";
                            $scope.$evalAsync();
                            resolve(result); 
                        }
    
                    }

                });
            })

        }
        /**
         * 
         * @param {string} parentPath //path to editor
         * @return {Array} unityModules 
         */

        

        function putDataIntoJSON(searchPaths, allUnity) {
            
            
            var res = {
                unity: allUnity,
                searchPaths: searchPaths,
            }
            try {

                
                //fs.writeFileSync(pathutil.join(_dir ,'config/unityPaths.json'), JSON.stringify(res));
                Globals.unity = allUnity;
                Globals.searchPaths = searchPaths;
                Globals.save();
                Globals.update();
                
                toastr.success("All information received");
                $scope.stop_button = "Close";
                $scope.stopBtnClass = "btn btn-success form-control search-btn"
                setTimeout(()=>{
                    $scope.progressBarWrapper = {
                        "display":"none"
                    };
                    $scope.$evalAsync();
                },1000); 
                $scope.$evalAsync();
                $window.stopScroll();

            } catch (e) {

                logError(e);
                
            }

        }

        function logError(error){
          
            toastr.error(error);
            console.log(error);
    
        }

        $scope.addPath = function () {

            // path = $window.newPath;
            // if(Globals.searchPaths.includes(path)){
            //   return toastr.error("such path already exists");
            // }
            // Globals.searchPaths.push(path);
            // Globals.save();
            // Globals.update();

        }

        $scope.removePath = function (e) {
            var tmp = Globals.searchPaths;
            var id = tmp.indexOf(e.key);
            if (id < 0) return toastr.error("Such path does not exists! deletion was unsuccessful");

            Globals.searchPaths.splice(id, 1);
            Globals.save();
            Globals.update();
            toastr.info("path removed");
        }

        // function walking(path,filter=[])
        // {
        //     return new promise(function(resolve,reject){
        //         walker = walk.walk(path, {filters: filter});
        //              //$scope.$evalAsync()
        //             walker.on("directories", function (root, dirs, next) {
        //             //if(STOP) return resolve(tmp);
        //             //debugger;
        //             // $scope.all_output += root +"\\"+ fileStats.name + "\n";
        //             // $scope.count2++;
        //             // $scope.$evalAsync()

        //             if(dirs.length==2)
        //             {
        //                 if(dirs[0].name == "Editor" && dirs[1].name== "MonoDevelop")
        //                 {

        //                     console.log(root+"\\"+dirs[0].name);
        //                     //walker = walk.walk(path.dirname(root),{filters: $scope.options.skipDirectories});

        //                     return resolve({
        //                         unityPath:root+"\\"+dirs[0].name,
        //                         skipPath:pathutil.basename(root)
        //                     })
        //                 }
        //             }
        //             next();

        //             // fs.readFile(fileStats.name, function () {
        //             //     $scope.all_output += root +"\\"+ fileStats.name + "\n";
        //             //     $scope.count2++;
        //             //     $scope.$evalAsync()

        //             //     if(fileStats.name.match(/(.html)$/))
        //             //     {
        //             //         //console.log("found: "+ root +"\\"+ fileStats.name);
        //             //         $scope.found_output += root +"\\"+ fileStats.name + "\n";
        //             //         $scope.count1++;
        //             //         $scope.$evalAsync()
        //             //         tmp.push(root +"\\"+ fileStats.name);
        //             //         //console.log(fileStats);
        //             //     }
        //             // next();
        //             // });
        //         });

        //         walker.on("end", function () {
        //             resolve();

        //         });

        //         walker.on("error", function (e) {
        //             reject(e);

        //         });
        //     });

        // }

    });