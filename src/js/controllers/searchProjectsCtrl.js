angular.module("App")

.controller('searchProjectsCtrl',function($scope,$window,$http,Globals){

    $http({
        method: 'GET',
        url: 'config/unityPaths.json'
    })
    .then(function (success) {
        $scope.dataToShow = success.data.projectPaths; 
    })
    .catch(function (reject) {
        console.warn("Error occured trying to retrieve file(in search project controller)");
    })


    
    $scope.savePaths = function(){
        $scope.paths = $window.paths;
        Globals.projectPaths = $scope.paths;
        Globals.save();
        Globals.update();
        toastr.success("paths saved");
    }

    $scope.removePath = function (e) {
        var tmp = Globals.projectPaths;
        var id = tmp.indexOf(e.key);
        if (id < 0) return toastr.error("Such path does not exists! deletion was unsuccessful");

        Globals.projectPaths.splice(id, 1);
        Globals.save();
        Globals.update();
        toastr.info("path removed");
    }
})