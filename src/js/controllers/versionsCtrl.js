var vsort = require('version-sort'); //need to delete this since this doesnt work at all
var child = require('child_process');
var projectGetter = require('./../nodejs/unityProjectGetter');
angular.module("App")

.controller("versionsCtrl",function($scope,$rootScope,Globals,$http,NgTableParams){
    //init
    var grouped = {};
    (function(){
        Globals.update();
        $scope.data = Globals.unity;

            //tmp = el.version.replace(/(Unity)|(\(32-bit\)|\(64-bit\)$)|\s/g,"");
        //console.log($scope.data);

        $scope.data.forEach(function(element){
            tmp = element.version.replace(/(Unity)|(\(32-bit\)|\(64-bit\)$)|\s/g,"");
            tmp = tmp.split(".");
            element.baseVersion = tmp[0]+"."+tmp[1];
        });

        //console.warn("<here>");
        //console.log($scope.data);


        //console.log(grouped);

        //$scope.group = grouped;

        //console.log($scope.group);
        $scope.options = createUsingFullOptions();
        //$scope.options.reload()

    })()



    function createUsingFullOptions() {
        var initialParams = {
          //  count: 5, // initial page size
            group:"baseVersion",
            page: 1, // show first page
            total: 1, // value less than count hide pagination
            count: 999, // count per page
            sorting: { version: 'desc' }
        };
        var initialSettings = {

            // page size buttons (right set of buttons in demo)
            counts: [],
            // determines the pager buttons (left set of buttons in demo)
            //paginationMaxBlocks: 13,
            //paginationMinBlocks: 2,
            groupOptions: {
              isExpanded: false
            },
            dataset: $scope.data,
        };
        return new NgTableParams(initialParams, initialSettings);
    }

    $scope.reverse = function(){
        console.log("s");
        $scope.options = new NgTableParams({}, {dataset: $scope.data.reverse()});
    }

    $scope.toggle = function () {
        $scope.options.settings().groupOptions.isExpanded = ($scope.options.settings().groupOptions.isExpanded) ? false : true;
        $scope.options.reload();
    }

    $rootScope.reload = function(){
        console.log("reloaded");
        $scope.options.reload();
    }

    $scope.launchUnity = function(path,options){
        /**
         * with arguments
         */
        if($scope.launch_arguments){
            toastr.info("argument: "+$scope.launch_arguments,"Launching unity");
            param = 'start "" "'+ path +'" '+$scope.launch_arguments;
            var ls = child.exec(param,function(err,stdout,stderr){
                if(err) toastr.error(err);
                if(stderr) toastr.error(stderr);
                if(!stdout) toastr.info("Unity closed");
                else{
                    toastr.warning(stdout.toString(),"wow, this has never happened before");
                    console.error("/-----------------------------------/\n" + stdout.toString() + "\n/-----------------------------------/")
                }
            })

        //without arguments
        }else{
            toastr.info("Launching unity");
            param = 'start "" "'+ path +'"';
            var ls = child.exec(param,function(err,stdout,stderr){
                if(err) toastr.error(err);
                if(stderr) toastr.error(stderr);
                if(!stdout) toastr.info("Unity closed");
                else{
                    toastr.warning(stdout.toString(),"wow, this has never happened before");
                    console.error("/-----------------------------------/\n" + stdout.toString() + "\n/-----------------------------------/")
                }
            })
        }
       
    }

    $scope.openExplorer = function(path){
       
       try{         
            child.exec('start "" "'+path+'"',function(err,stdout,stderr){
                if(err) toastr.error(err);
                if(stderr) toastr.error(stderr);
            });
        }catch(e){
            console.error("handled error\n" + e);
        }


    }

    

    $scope.load_projects = function(){
        console.log("load projects called");
        
        if(Globals.projectPaths.length<1){
            toastr.warning("project path(s) is/are not set");
            return;
        }

        $scope.projects = [];
        $scope.projectsLoaded = false;
        projectGetter.start();
        var ev = projectGetter.events;
        ev.removeAllListeners(); //remove listeners so there wont be dupicates

        ev.on('started',(msg)=>{
            console.log(msg);
        })
        
        ev.on("data",(data)=>{
                     
            $scope.projects = JSON.parse(data);
            if($scope.projects.length<1){
                toastr.error("not a single project found in specified directory");
            }
            $scope.projectsLoaded = true;
            $scope.$evalAsync();
        })

        ev.on("close",()=>{
            console.log("process exited");
        })

        ev.on("error",(err)=>{
            logErr(err);
        })
    }

    function logErr(err){
        console.error(err);
        toastr.error(err)
    }

    $scope.addProjectAsLaunchOption = function(pathToProj,title){
        $scope.selectedProjectTitle = title;
        $scope.letRemoveSelectedProject = true;
        $scope.selectProjectStyle = {
            "font-weight": "bold",
            "border":"2px solid black"
        };

        //setting launch argument
        $scope.launch_arguments = '-projectPath "'+ pathToProj + '"';
        $scope.$evalAsync()
        
        //console.log($scope.selectedProjectTitle);

    }

    $scope.removeProjectAsLaunchOption = function(){
        $scope.selectedProjectTitle='Select project';
        $scope.letRemoveSelectedProject = false;
        $scope.selectProjectStyle = {};
        //setting launch argument
        $scope.launch_arguments = "";
        $scope.$evalAsync()
    }

});
