angular.module("App")

    .controller("initCtrl", function ($scope, $http, Globals) {
        //check if app holds any info about unity paths
        $http({
                method: 'GET',
                url: 'config/unityPaths.json'
            })
            .then(function (success) {
                if(!success.data.unity){
                    window.location.hash = "#!/search";
                    return;
                }
                Globals.unity = success.data.unity;
                Globals.searchPaths = success.data.searchPaths;
                Globals.projectPaths = success.data.projectPaths;
                Globals.save();
                Globals.update();
                window.location.hash = "#!/main";
                //window.location.hash = "#!/search";
                console.log("eyyy,unity paths exist so showing main.html");

            })
            .catch(function (reject) {
                console.log("Error occured trying to retrieve file");
                window.location.hash = "#!/search";
            })
    })