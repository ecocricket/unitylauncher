var promise = require("promise");
var fs      = require("fs");
var child   = require("child_process");
var pathutil= require("path");
var unityModuleGetter = require("../nodejs/unityModuleGetter");

angular.module("App")

.controller("mainCtrl",function($scope,Globals,$rootScope){
    /**
     * REFRESH PART
     */
    var currUnitys, searchPaths, results;

    $scope.refresh = function(){
        Globals.update(); //get latest info from json
        currUnitys  = Globals.unity; 
        searchPaths = Globals.searchPaths;

        results = {
            deleted:0,
            found:0
        };
        //async operations
        checkForUninstalled(currUnitys) //verify all unitys
            .then(getAllUnity) //search paths for unity and send to compare func
            .then(compare) // compare if there are more paths etc..
            .then(handleRes)
            .catch(handleErr)
    }
    /**
     * 
     * @param {Array} currUnitys //current unitys from unityPaths.json 
     */
    function checkForUninstalled(currUnitys){
        return new promise((resolve, reject)=>{
            try{
                currUnitys.forEach(unity=>{
                    if(!fs.existsSync(unity.executablePath)){
                        currUnitys.splice(currUnitys.indexOf(unity),1);
                        results.deleted++;
                    }
                })

                resolve();
            }catch(e){
                reject(e);
            }
            
        })
    }

    /**
     * 
     * @param {Array} searchPaths 
     */
    function getAllUnity(){
        return new promise((resolve,reject)=>{
            try{
                var countDown = searchPaths.length;
                var resPaths = []; //store all found unitys
    
                searchPaths.forEach(function (base) {
    
                    var dirs = fs.readdirSync(base)
                    
                    dirs.forEach(function (element) {
    
                        //for unity 5.0 and up
                        var check1 = pathutil.join(base, element, "Editor\\Unity.exe");
                        //for unity 4.7 and lower
                        var check2 = pathutil.join(base, element, "Unity.exe");
    
                        if (fs.existsSync(check1)) {
                            resPaths.push(pathutil.join(base, element, "Editor"));
                            
                        } else if (fs.existsSync(check2)) {
                            resPaths.push(pathutil.join(base, element));
                        }                  
    
                    });
                    // if all paths have been iterated, resolve promise
                    countDown--;
                    if(countDown<=0){
                        resolve(resPaths);
                    }
                    
    
                })
            
            }catch(e){
                reject(e);
            }
        })
    }

    

    /**
     * 
     * @param {Array} newPaths // newest unity info gathered (paths)
     */
    function compare(newPaths){
        var currPaths = [];
        return new promise((resolve, reject)=>{
            try{
                currUnitys.forEach((element)=>{
                    currPaths.push(element.parentPath);
                });

                newPaths.forEach((newPath)=>{
                    if(!currPaths.includes(newPath))
                    {
                        var command = 'powershell -command "& {&Get-ItemPropertyValue -Path \'' + newPath + "\\Uninstall.exe" + '\' -name \'VersionInfo\' | ConvertTo-Json}"';
                        var data = child.execSync(command)

                        data = data.toString("utf-8").replace(/\n|\r/g, "");
                        data = JSON.parse(data);

                        var modules = unityModuleGetter(newPath);

                        currUnitys.push({
                            parentPath: newPath,
                            executablePath: pathutil.join(newPath, "Unity.exe"),
                            path: data.FileName,
                            rawVersion: data.ProductName,
                            version: data.ProductName.replace(/(Unity)|(\(32-bit\)|\(64-bit\)$)|\s/g,""),
                            modules: modules
                        })
                        
                        results.found++;                                       
                    }
                })

                resolve(currUnitys);
            }catch(e){
                reject(e);
            }
        });
    }

   
    function handleRes(newUnitys){
        Globals.unity = newUnitys;
        Globals.save();
        Globals.update();
        location.hash="#!/hm";
        location.hast="#!/main";
        toastr.success("found new:"+results.found+"<br>uninstalled:"+results.deleted+"","Refresh succesfull");
        
        //$scope.$apply();
    }

    function handleErr(err){
        console.warn(err);
        toastr.error(err);
    }

    $scope.revertJson = function(){
        Globals.unity = [];
        Globals.searchPaths = [];
        Globals.projectPaths = [];
        Globals.save();
        Globals.update();
        location.reload();
        toastr.success("json reseted");
    }

    
   

    //end of refresh code
});